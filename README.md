# README #

These are the slides of a seminar given to the student of the first year
physics's laboratory, and describing the
[plasduino](http://pythonhosted.org/plasduino/)
software and its applications in the physics lab.

Slides are in italian, but pretty easy to understand (I hope)


### How do I get set up? ###

* Just download everything
* Open plasduino_seminario.pdf with your favorite viewer
* To compile, just type 'make'
* Or use 'pdflatex plasduino_seminario'


