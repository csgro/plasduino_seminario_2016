all:
	pdflatex plasduino_seminario

clean:
	rm -f *~ *.aux *.log *.nav *.out *.snm *.toc *.vrb

cleanall:
	make clean
	rm -f *.pdf
